# Custom Docker Build Pipeline Image

To speed up our build pipelines and save usage time we create custom container images with everything we need baked in.

## Recommended documentation

* [Pushing and Pulling to and from Docker Hub](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html)
* [Pushing a Docker container image to Docker Hub](https://docs.docker.com/docker-hub/repos/#pushing-a-docker-container-image-to-docker-hub)

## Hosting

Nothing propitiatory or custom is in our docker images so we can host the images publicly, but for simplicity and tight integration we are using GitLab Docker Container Repository.

__Url:__ [](registry.gitlab.com/choose-shift/mbti-challenge)

## Basics of manually building and pushing

```
docker build --pull --tag=choose-shift/mbti-challenge build/php/
docker login --username=USERNAME
docker push choose-shift/mbti-challenge:php
```

## Pipeline integration

Manually building of container images should be unnecessary as we are using GitLab build pipelines to build and update push our containers automatically.

## Local usage

If you would like to use and run these Docker images locally you have two choices.

1. Pull from Gitlab registry **(recommended)** EG: `docker pull choose-shift/mbti-challenge:php`
1. Build locally EG: `docker build --pull --tag=choose-shift/mbti-challenge build/php/`

## Tags

The tag name is used to manage multiple images with different configs and purposes.

**Current tags**

- php
- node
