<div align="center"><img src="images/logo-banner-dark.png" title="Persona Touchstone - Justin Fossey - Shift Technical Challenge" /></div>

# Persona Touchstone - Justin Fossey - Shift Technical Challenge

The purpose of the **Persona Touchstone** is to provide a tool that makes it easy to perform __Myers-Briggs Type Indicator__ (**MBTI**) questionnaires and that is easy to use and to assess the results.

## Purpose

The primary purpose of this project is to show what knowledge and experience **Justin Fossey** has as a full-stack web developer.

Several changes and elements of this project were done specifically to show knowledge, understanding and experience in a specific topic or concept. For this reason, the methods and ideas in this project should not be considered best practices or recommended for production use as this project is a demonstration only.

## Scope / Brief

The original brief of this project is the property of Shift and is not distributed as part of this project.

## Technologies Used

**Note:** Items marked with a star **(*)** are items that were used for the first time in this project.

- Laravel PHP Framework
- Docker
- GitLab CI Pipelines
- NodeJs
- Bash
- Bower
- React **(*)**
- React Router **(*)**
- Markdown
- Spectre.css
- Line-Awesome

### Other Technologies Considered

The following where technologies considered but were decided against for time and efficiency sake in delivering in a reasonable time-frame.

- TypeScript
- MobX

## Changelog

A [changelog.md](changelog.md) file was created expressly to help guide and make assessment and evaluation of code simpler and easier.

To save time it is recommended to start by reviewing the changelog as it will help guide any reviewer to the key areas and highlight the most important aspects.

## Requirements

1. Git
1. Linux/Unix Web server (Apache, Nginx)
1. PHP version 7.2 or 7.3 and above (7.1 may work but is un-tested)
1. MySQL (Sqlite my work but is un-tested)
1. Composer
1. Node
1. Yarn
1. Bower
1. Bash

## Install

1. clone git repository
1. Point a web server vhost to the `src/public` directory
1. copy the `.env.example` file to `.env` and edit accordingly EG: Database details
1. from project root run installer EG: `./install.sh`
1. from project root run code checker EG: `./code-check.sh`

## Omissions

1. The UI is not very polished and was not a focus point.
1. The results were calculated based on my understanding of the brief and the sample data, as I have not worked with MBTI data before I cant be 100% sure my formula is correct.
1. The validation of the questions is incomplete. If you do not submit all the questions you can easily generate an error. __(Follow up: This is partly fixed in recent commits relating to db migrations)__
1. There is technically a data leak vulnerability in that you can view other users results by editing the ids on the results page EG: `results/1/submission/1`. I did consider doing something about this, but decided to leave it as it can be used to view other results and to help verify accuracy of other results, notably the seeded data. This would never be allowed in a production system.

## Docs

List of key markdown source documents.

- [readme.md](readme.md)
- [changelog.md](changelog.md)
- [build/README.md](build/README.md)
- [src/packages/README.md](src/packages/README.md)

## Database Schema

Database migrations and example data seeders exist using the below database schema.

Once your database is configured in your `src/.env`, you can install the database schema using `php artisan migrate --seed`. Run from inside the `src` directory.

![](screenshots/db-schema.png)

## Tests & Static Analysis

To enforce a high quality of code and a common standard, several tests and static analysis tools have been set up and will run when pushing to origin using GitLab Pipelines.

To run all the checks and tests locally run the `code-check.sh` shell script.

![](screenshots/code-check.gif)

## Screen-shots

### Home page

![](screenshots/home.jpg)

### Help page

![](screenshots/help.jpg)

## Result

![](screenshots/result.jpg)

### Questions page

![](screenshots/questions.jpg)

## License

The Persona Touchstone is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).