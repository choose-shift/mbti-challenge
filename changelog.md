# Change Log

The following is a high-level changelog recording only important or significant changes.

The purpose of this changelog is not that of a traditional changelog but rather to provide a summary of changes worth assessing or reviewing.

1. Basic __README__ and __MIT License__ added
1. Standard __Laravel App__ installed under `src/`
1. Added `build/` directory with a **node** and **php** Dockerfile
1. Added __GitLab__ CI YAML config to build and push our container images using purpose specific tags
1. Setup static analysis, coding standards and unit testing
    1. [PHP Easy Coding Standards](https://github.com/Symplify/EasyCodingStandard)
    1. PHP Unit
    1. ESLint
    1. StyleLint
1. Added local shell script for easy testing and linting for developers @see `code-check.sh`
1. Added __GitLab__ testing pipelines to run __NODE__ and __PHP__ tests, linting and static analysis for each push/merge
1. Setup Bower package manager for assets we dont want to manage as node modules
    1. [Spectre.css](https://picturepan2.github.io/spectre/) (lightweight CSS framework)
    1. [Line-Awesome](https://icons8.com/line-awesome) (Font awesome alternative)
1. Rename and brand `app/` folder to `shift/` and update Laravel application container and composer PSR-4 namespace
1. Added Support global function helpers to be loaded by composer as a static include
1. Updated App service provider to support dynamic loading of Laravel __macros__ found in `App\Support\Macros\*.macro.php`
1. Setup **React** and **React Router** application in `src/resources/js` combined with `App\Http\Controllers\SinglePageController` to provide simplified __SPA__ functionality
1. Add custom packages __namespace__, for more info see readme at `src/packages/README.md`
1. Add custom package **PageMaker** for rendering static content as markdown
1. Created `PageConent` react component that fetches static content from page maker routes
1. Add __Questions__ and __MbtiQuestion__ item React components that fetch and display questions retrieved from the bankend
1. Database __migrations__ were created based on designed [DB Schema](screenshots/db-schema.png) including users, questions, answers and submissions tables
1. __Eloquent__ models were added to `App` for User, Question, Answer and Submission
1. Database seeders were developed to import provided example data from CSV files
1. MbtIQuestions PHP package was added that manages the fetching of questions lists and processing new submitted answers
1. The [laravel-fractal](https://github.com/spatie/laravel-fractal) package was added for managing API end-point output
1. Added Custom request for validating answer submissions.
1. Create a service class for persisting data and calculating results.
1. Added __AnswerSubmit__ and __Results__ react components and to use __MbtIQuestions__ package API endpoint.
