<?php

declare(strict_types=1);

namespace App\Http\Controllers;

// Framework
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View as ViewContract;

// Aliases
use View;

/**
 * This is our Single Page Controller and the default for all requests.
 */
class SinglePageController extends Controller
{
    /**
     * Default wild card index route.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request): ViewContract
    {
        return View::make('app');
    }
}
