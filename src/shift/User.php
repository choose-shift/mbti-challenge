<?php

declare(strict_types=1);

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
    ];

    /**
     * Get the submissions for the user.
     *
     * @return  Relation
     */
    public function submissions(): Relation
    {
        return $this->hasMany('App\Submission');
    }
}
