<?php

declare(strict_types=1);

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Submission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'result',
    ];

    /**
     * Get the user that this submission belongs to.
     *
     * @return  Relation
     */
    public function user(): Relation
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the answers for the question.
     *
     * @return  Relation
     */
    public function answers(): Relation
    {
        return $this->hasMany('App\Answer');
    }
}
