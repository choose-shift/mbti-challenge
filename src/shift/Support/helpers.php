<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Shortcuts and global helper functions
|--------------------------------------------------------------------------
|
| An custom function that needs to be available in the global context.
|
*/

// Framework
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Detailed Die and Dump.
 *
 * Dump the passed variables and end the script, but with a mini stack trace.
 *
 * @return void
 */
function ddd()
{
    app('Illuminate\Contracts\Http\Kernel')->terminate(request(), response());

    array_map(function ($x) {
        (new VarDumper())->dump($x);
    }, func_get_args());

    print simple_call_trace(30);

    die(1);
}

/**
 * Provide a mini Call Stack Trace, used by ddd().
 *
 * @param  integer $limit
 *
 * @return string
 */
function simple_call_trace(int $limit = 0)
{
    $e     = new Exception();
    $trace = explode("\n", $e->getTraceAsString());

    array_shift($trace); // remove call to this method
    array_pop($trace);   // remove {main}

    $length = count($trace);
    $result = [];

    for ($i = 0; $i < $limit; $i++) {
        if ($length == $i) {
            break;
        }

        // replace '#someNum' with '$i)', set the right ordering
        $spacePos = (int)strpos($trace[$i], ' ');
        $result[] = ($i + 1) . ')' . substr($trace[$i], $spacePos);
    }

    return "<pre>\t" . implode("\n\t", $result) . '</pre>';
}

/**
 * Alias for the macro Str::formatString()
 *
 * @param  string   $string
 * @param  array    $args
 *
 * @return string
 */
function t(string $string, array $args = [])
{
    return Str::formatString($string, $args);
}

/**
 * Parse a collection and return all the data as a Collection. This should only
 * be used on small data sets.
 *
 * @param  string $path
 * @param  bool|boolean $firstRowHeadings
 * @param  bool|boolean $lowerHeadings
 *
 * @return Collection
 */
function collect_csv(string $path, bool $firstRowHeadings = true, bool $lowerHeadings = true): Collection
{
    // Check we can open the file handler resource
    if (($handle = fopen($path, "r")) === false) {
        throw new RuntimeException(t('Permissions denied, cant read :path', [
            ':path' => $path,
        ]));
    }

    $rowCount = 0;
    $headings = null;
    $rows     = [];

    // Create an array of each row
    while (($data = fgetcsv($handle, 0, ",")) !== false) {
        // Always increment row count
        $rowCount++;

        // Check if we need capture the headings
        if ($firstRowHeadings === true && $rowCount === 1) {
            $headings = $lowerHeadings ? array_map('strtolower', (array)$data) : $data;
            continue;
        }

        // Build a row using headings if they exist
        $rows[] = empty($headings) ? $data : array_combine($headings, (array)$data);
    }

    fclose($handle);

    return collect($rows);
}
