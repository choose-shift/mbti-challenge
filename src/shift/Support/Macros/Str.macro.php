<?php

declare(strict_types=1);

/**
 * Macro: Str::formatString()
 *
 * Build a string based on named tokens. String token modifier are supported.
 *
 * @ = escape
 * : = raw
 *
 * @param   string  $string
 * @param   array   $args
 *
 * @return  string
 */
Illuminate\Support\Str::macro('formatString', function (string $string, array $args = []) {
    // Transform arguments before inserting them.
    foreach ($args as $key => $value) {
        switch ($key[0]) {
            case '@':
            default:
                // Escaped and placeholder.
                $args[$key] = e($value);
                break;

            case ':':
                // Pass-through.
        }
    }
    return strtr($string, $args);
});
