<?php

declare(strict_types=1);

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'submission_id',
        'question_id',
        'value',
    ];

    /**
     * Get the question that this answer belongs to.
     *
     * @return  Relation
     */
    public function question(): Relation
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * Get the submission that this answer belongs to.
     *
     * @return  Relation
     */
    public function submission(): Relation
    {
        return $this->belongsTo('App\Submission');
    }
}
