<?php

declare(strict_types=1);

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question',
        'dimension',
        'direction',
        'meaning',
    ];

    /**
     * Get the answers for the question.
     *
     * @return  Relation
     */
    public function answers(): Relation
    {
        return $this->hasMany('App\Answer');
    }
}
