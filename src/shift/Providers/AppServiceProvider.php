<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Dynamically Load macros
        if ($macros = glob(app_path('Support/Macros/*.macro.php'))) {
            foreach ($macros as $filename) {
                require_once($filename);
            }
        }
    }
}
