import "./bootstrap";
import React from "react";
import ReactDom from "react-dom";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// Import App root component
import App from "./components/App";

// Fetch master child components object list that App must consume and render
import components from "./components";

// Bind and render our application
ReactDom.render(<Router><App components={components} /></Router>, document.getElementById("app-root"));
