import React from "react";
import axios from "axios";

import MbtiQuestion from "./MbtiQuestion";
import AnswersSubmit from "./AnswersSubmit";

class Questions extends React.Component {
    /**
     * Create component instance
     */
    constructor (props) {
        super(props);

        this.state = {
            questions: [],
            error: null,
            selected: {}
        };

        // This binding is necessary to make `this` work in the callback
        this.fetchQuestions             = this.fetchQuestions.bind(this);
        this.setQuestions               = this.setQuestions.bind(this);
        this.getQuestions               = this.getQuestions.bind(this);
        this.handleError                = this.handleError.bind(this);
        this.handleQuestionOptionChange = this.handleQuestionOptionChange.bind(this);
    }

    /**
     * Fetch from backend the list of questions.
     *
     * @see  componentDidMount
     *
     * @return void
     */
    fetchQuestions () {
        axios.get("mbti/questions")
            .then(result => { this.setQuestions(result.data); })
            .catch(error => { this.handleError(error); });
    }

    /**
     * Set list of questions in state.
     *
     * @param Array questions
     *
     * @return void
     */
    setQuestions (questions) {
        this.setState(state => ({
            questions: questions.data,
            error: null,
            selected: state.selected
        }));
    }

    /**
     * Callback for child events to use when question options are selected.
     *
     * @param  Number questionNumber
     * @param  Number optionNumber
     *
     * @return void
     */
    handleQuestionOptionChange (questionNumber, optionNumber) {
        const selectedOptions = this.state.selected;
        selectedOptions[questionNumber] = optionNumber;

        this.setState(state => ({
            questions: state.questions,
            error: null,
            selected: selectedOptions
        }));
    }

    /**
     * Build an array of question components.
     *
     * @return Array
     */
    getQuestions () {
        // Show loading when no questions are leaded
        if (this.state.questions.length == 0) {
            return <h2 className="text-center"><i className="la la-spinner" /> Loading...</h2>;
        }

        // Callback for building individual questions components
        const buildQuestion = function (item, index) {
            const number = index + 1;

            return (
                <div key={"q-" + index} className="column col-12">
                    <MbtiQuestion question={item.question} id={item.id} number={number} onSelectOption={this.handleQuestionOptionChange} />
                </div>
            );
        }.bind(this);

        // Build array of question components
        const items = this.state.questions.map(buildQuestion);

        // Return questions JSX with child components
        return (
            <div id="questions-wrapper" className="container">
                <div className="columns">{items}</div>
            </div>
        );
    }

    /**
     * Handle ajax errors.
     *
     * @param  event error
     *
     * @return void
     */
    handleError (error) {
        const errorMessage = <div className="text-center error"><i className="la la-exclamation-triangle" />Sorry, there was a problem fetching your questions.</div>;

        this.setState(state => ({
            questions: state.questions,
            error: errorMessage,
            selected: state.selected
        }));

        console.log(error);
    }

    /**
     * Component did mount life-cycle method.
     *
     * @return void
     */
    componentDidMount () {
        // Fetch our questions from backend
        this.fetchQuestions();
    }

    /**
     * Render life-cycle method.
     *
     * @return JSX
     */
    render () {
        const questions    = this.getQuestions();
        const errorMessage = this.state.error;

        return (
            <div>
                <h1 className="text-center">Discover Your Prespective</h1>
                <p className="text-center">Please complete the below questions to get a detailed report of your lense on the world.</p>
                {errorMessage}
                {questions}
                <AnswersSubmit answers={this.state.selected} />
            </div>
        );
    }
}

// Export Application component
export default Questions;
