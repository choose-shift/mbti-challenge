import React from "react";
import PropTypes from "prop-types";

class MbtiQuestion extends React.Component {
    /**
     * Create component instance
     */
    constructor (props) {
        super(props);

        this.state = {
            option: null
        };

        // This binding is necessary to make `this` work in the callback
        this.setOption          = this.setOption.bind(this);
        this.getOptions         = this.getOptions.bind(this);
        this.isCheckedOption    = this.isCheckedOption.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }

    /**
     * Get a range of numbers for let length.
     *
     * @param  number length
     *
     * @return Array
     */
    range (length) {
        return Array.from({ length: length }, (x, i) => i + 1);
    }

    /**
     * Check if a option number is the currently selected option set in state.
     *
     * @param  Number  optionNumber
     *
     * @return Boolean
     */
    isCheckedOption (optionNumber) {
        return this.state.option === optionNumber.toString();
    }

    /**
     * Set a option number in component state.
     *
     * @param Number optionNumber
     */
    setOption (optionNumber) {
        this.setState(() => ({
            option: optionNumber
        }));

        this.props.onSelectOption(this.props.id, optionNumber);
    }

    /**
     * Handle onChange event on a radio option.
     *
     * @param  Event event
     *
     * @return void
     */
    handleOptionChange (event) {
        this.setOption(event.target.value);
    }

    /**
     * Build an array of question options form fields.
     *
     * @return Array
     */
    getOptions () {
        // Unique name for radios form field
        const name = "q-" + this.props.number;

        // Callback for building form field option
        const buildOption = function (optionNumber) {
            const isChecked   = this.isCheckedOption(optionNumber);

            return (
                <label key={optionNumber} className="form-radio">
                    <input type="radio" name={name} value={optionNumber} checked={isChecked} onChange={this.handleOptionChange} />
                    <i className="form-icon" />
                </label>
            );
        }.bind(this);

        return this.range(7).map(buildOption);
    }

    /**
     * Render life-cycle method.
     *
     * @return JSX
     */
    render () {
        const number = this.props.number;
        const question = this.props.question;
        const questionOptions = this.getOptions();

        return (
            <div className="card mbti-question">
                <div className="card-header">
                    <div className="card-title h5"><span className="chip">{number}</span> {question}</div>
                </div>
                <div className="card-body">
                    <div className="container">
                        <div className="columns">
                            <div className="column col-2"><span className="text-error">Disagree</span></div>
                            <div className="column col-8">
                                <div className="form-group"><div className="input-group">{questionOptions}</div></div>
                            </div>
                            <div className="column col-2"><span className="text-success">Agree</span></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Define our Props types for validation of our property variable types
MbtiQuestion.propTypes = {
    id: PropTypes.number,          // Question unique id
    number: PropTypes.number,      // Display question number
    question: PropTypes.string,    // Question text
    onSelectOption: PropTypes.func // Parent option change callback
};

// Export Application component
export default MbtiQuestion;
