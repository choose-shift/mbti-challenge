import React from "react";
import axios from "axios";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";

class AnswersSubmit extends React.Component {
    /**
     * Create component instance
     */
    constructor (props) {
        super(props);

        this.state = {
            email: "",
            error: null,
            redirect: null
        };

        // This binding is necessary to make `this` work in the callback
        this.handleEmailChange  = this.handleEmailChange.bind(this);
        this.handleAnswerSubmit = this.handleAnswerSubmit.bind(this);
    }

    /**
     * Handle when email field changes, updating state.
     *
     * @param  Event event
     *
     * @return void
     */
    handleEmailChange (event) {
        this.setState({
            email: event.target.value,
            error: null,
            redirect: null
        });
    }

    /**
     * Handle for the form submitting of answers.
     *
     * @param  Event event
     *
     * @return void
     */
    handleAnswerSubmit (event) {
        // Take over the form submit
        event.preventDefault();

        // Validate if we have an email address
        if (this.state.email == "") {
            this.setState({
                email: "",
                error: <div className="m-2"><span className="label label-error">Email Required:</span> Please provide your email address before submitting your answers.</div>,
                redirect: null
            });

            // Stop submit on error
            return;
        }

        // Format POST data values
        const dataPayload = {
            email: this.state.email,
            answers: this.props.answers
        };

        // Do Ajax post
        axios.post("/mbti/save", dataPayload)
            .then(result => { this.redirectToResults(result.data); })
            .catch(error => { this.handleError(error); });
    }

    /**
     * Redirect to results page based on server response.
     *
     * @return void
     */
    redirectToResults (result) {
        const path = "results/" + result.user + "/submission/" + result.submission;

        this.setState(state => ({
            email: state.email,
            error: null,
            redirect: <Redirect to={path} />
        }));
    }

    /**
     * Handle ajax errors.
     *
     * @param  event error
     *
     * @return void
     */
    handleError (error) {
        const errorMessage = <div className="text-center error m-2"><i className="la la-exclamation-triangle" />Sorry, there was a problem processing your questions.</div>;

        this.setState(state => ({
            email: state.email,
            error: errorMessage,
            redirect: null
        }));

        console.log(error);
    }

    /**
     * Render life-cycle method.
     *
     * @return JSX
     */
    render () {
        const errorMessage = this.state.error;
        const redirect     = this.state.redirect;

        return (
            <div className="card mbti-question">
                <div className="card-header">
                    <div className="card-title h5">Your Email</div>
                </div>
                <div className="card-body">
                    <form className="text-center" onSubmit={this.handleAnswerSubmit}>
                        {errorMessage}
                        <div className="form-group">
                            <input className="form-input" type="email" id="user-email" value={this.state.email} onChange={this.handleEmailChange} placeholder="you@example.com" />
                        </div>
                        <button className="btn btn-primary input-group-btn">Save & Continue</button>
                    </form>
                </div>
                {redirect}
            </div>
        );
    }
}

// Define our Props types for validation of our property variable types
AnswersSubmit.propTypes = {
    answers: PropTypes.object // The select options
};

// Export Application component
export default AnswersSubmit;
