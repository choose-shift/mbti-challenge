import React from "react";

/**
 * AppFooter lays out footer content.
 */
function AppFooter () {
    return  (
        <div id="footer-wrapper">
            <section className="container grid-lg">
                <div id="footer" className="text-center">Persona Touchstone Powered By ReactJS</div>
            </section>
        </div>
    );
}

// Export Application component
export default AppFooter;
