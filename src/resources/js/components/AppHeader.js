import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

/**
 * AppHeader functional component that lays out the header and navigation
 * router links.
 */
function AppHeader () {
    return  (
        <div id="header-wrapper">
            <section className="container grid-lg">
                <header className="navbar">
                    <section className="navbar-section">
                        <Link to="/">
                            <h1 id="logo" className="navbar-brand"><i className="la la-lightbulb-o" />Persona Touchstone</h1>
                        </Link>
                    </section>
                    <section className="navbar-section">
                        <Link className="btn btn-link" to="/">Home</Link>
                        <Link className="btn btn-link" to="/questions">Questions</Link>
                        <Link className="btn btn-link" to="/help">Help</Link>
                    </section>
                </header>
            </section>
        </div>
    );
}

// Export Application component
export default AppHeader;
