import React from "react";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

/**
 * AppContent functional component that lays out router sub components.
 */
function AppContent (props) {
    return  (
        <div id="content-wrapper">
            <section className="container grid-lg">
                <div id="content">
                    <Route path="/" exact component={props.components.Home} />
                    <Route path="/questions" component={props.components.Questions} />
                    <Route path="/results/:user/submission/:submission" component={props.components.Results} />
                    <Route path="/help" component={props.components.Help} />
                </div>
            </section>
        </div>
    );
}

// Define our Props types for validation of our property variable types
AppContent.propTypes = {
    components: PropTypes.object // Object of child components
};

// Export Application component
export default AppContent;
