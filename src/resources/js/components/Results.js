import React from "react";
import axios from "axios";
import PropTypes from "prop-types";

class Results extends React.Component {
    /**
     * Create component instance
     */
    constructor (props) {
        super(props);

        this.state = {
            answer: ""
        };

        // This binding is necessary to make `this` work in the callback
        this.fetchResult = this.fetchResult.bind(this);
        this.getResult   = this.getResult.bind(this);
    }

    /**
     * Fetch from backend the submission result.
     *
     * @see  componentDidMount
     *
     * @return void
     */
    fetchResult (user, submission) {
        axios.get("/mbti/" + user + "/result/" + submission)
            .then(result => { this.setResult(result.data); })
            .catch(error => { this.handleError(error); });
    }

    /**
     * Set the ajax result in state.
     *
     * @param object result
     */
    setResult (result) {
        this.setState(() => ({
            answer: result.result
        }));
    }

    /**
     * Build result display component.
     *
     * @return JSX
     */
    getResult () {
        if (this.state.answer.length) {
            return (
                <strong>{this.state.answer}</strong>
            );
        }

        return <strong>Loading..</strong>;
    }

    /**
     * Handle ajax errors.
     *
     * @param  event error
     *
     * @return void
     */
    handleError (error) {
        const errorMessage = <div className="text-center error"><i className="la la-exclamation-triangle" />Sorry, there was a problem fetching your questions.</div>;

        this.setState(() => ({
            answer: "",
            error: errorMessage
        }));

        console.log(error);
    }

    /**
     * Component did mount life-cycle method.
     *
     * @return void
     */
    componentDidMount () {
        // Get our url params
        const { user, submission } = this.props.match.params;

        // Fetch our questions from backend
        this.fetchResult(user, submission);
    }

    /**
     * Render life-cycle method.
     *
     * @return JSX
     */
    render () {
        const result = this.getResult();
        const errorMessage = this.state.errorMessage;

        return (
            <div>
                <h1 className="text-center">Your Prespective is {result}</h1>
                {errorMessage}
            </div>
        );
    }
}

// Define our Props types for validation of our property variable types
Results.propTypes = {
    match: {
        params: {
            user: PropTypes.number,
            submission: PropTypes.number
        }
    }
};

// Export Application component
export default Results;
