import React from "react";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

/**
 * Root Application class component that sets up our routes.
 */
class App extends React.Component {
    render () {
        // Get all child components we need here
        const AppHeader = this.props.components.AppHeader;
        const AppContent = this.props.components.AppContent;
        const AppFooter = this.props.components.AppFooter;

        return (
            <div id="content-wrapper">
                <section className="container grid-lg">
                    <AppHeader />
                    <AppContent components={this.props.components} />
                    <AppFooter />
                </section>
            </div>
        );
    }
}

// Define our Props types for validation of our property variable types
App.propTypes = {
    components: PropTypes.object // Object of child components
};

// Export Application component
export default App;
