import React from "react";
import axios from "axios";
import PropTypes from "prop-types";

/**
 * Page Content class component that fetches and renders static content from
 * our backend.
 */
class PageContent extends React.Component {
    /**
     * Create component instance
     */
    constructor (props) {
        super(props);

        this.state = {
            pageContent: ""
        };

        // This binding is necessary to make `this` work in the callback
        this.fetchPage       = this.fetchPage.bind(this);
        this.setPage         = this.setPage.bind(this);
        this.renderedContent = this.renderedContent.bind(this);
        this.handleError     = this.handleError.bind(this);
    }

    /**
     * Fetch the current page by name from backend.
     *
     * @param  string pageName
     *
     * @return void
     */
    fetchPage (pageName) {
        axios.get("/page/" + pageName + "/view")
            .then(result => { this.setPage(result.data); })
            .catch(error => { this.handleError(error); });
    }

    /**
     * Set page content state.
     *
     * @return void
     */
    setPage (newPageContent) {
        this.setState(() => ({
            pageContent: newPageContent
        }));
    }

    /**
     * Rendered content form last rendered page in state.
     *
     * @type string
     */
    renderedContent () {
        if (this.state.pageContent) {
            const content = {
                __html: this.state.pageContent
            };
            return <div dangerouslySetInnerHTML={content} />;
        }

        return <h2 className="text-center"><i className="la la-spinner" /> Loading...</h2>;
    }

    /**
     * Handle errors to display a not found message when we dont get a
     * valid result from ajax page fetch.
     *
     * @param  object error
     *
     * @return void
     */
    handleError (error) {
        const pageContent = <h2 className="text-center"><i className="la la-exclamation-triangle" />Sorry, Page not found</h2>;

        this.setPage(pageContent);
        console.log(error);
    }

    /**
     * Component did mount life-cycle method.
     *
     * @return void
     */
    componentDidMount () {
        // Fetch our content based on the page prop passed from parent
        this.fetchPage(this.props.page);
    }

    /**
     * Render life-cycle method.
     *
     * @return JSX
     */
    render () {
        return (
            <div className="page-content">
                <section>{this.renderedContent()}</section>
            </div>
        );
    }
}

// Define our Props types for validation of our property variable types
PageContent.propTypes = {
    page: PropTypes.string // string page name
};

// Export Application component
export default PageContent;
