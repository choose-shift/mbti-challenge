import React from "react";

// Main App Components
import AppHeader from "./components/AppHeader";
import AppContent from "./components/AppContent";
import AppFooter from "./components/AppFooter";
// Feature Components
import Questions from "./components/Questions";
import Results from "./components/Results";
import PageContent from "./components/PageContent";

// Static content components based on PageContent component
const Home = () => <PageContent page="home" />;
const Help = () => <PageContent page="help" />;

/**
 * Object list all main child components
 *
 * @type {Object}
 */
const components = {
    Home: Home,
    Help: Help,
    Questions: Questions,
    Results: Results,
    AppHeader: AppHeader,
    AppContent: AppContent,
    AppFooter: AppFooter
};

// Default export
export default components;
