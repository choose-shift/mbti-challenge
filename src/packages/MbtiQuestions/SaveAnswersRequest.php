<?php

declare(strict_types=1);

namespace MbtiQuestions;

// Vendor
use Illuminate\Foundation\Http\FormRequest;

class SaveAnswersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Some basic validation
        // Could do more to check all the questions are answers and handle.
        return [
            'email'   => 'required|email',
            'answers' => 'required|array',
        ];
    }
}
