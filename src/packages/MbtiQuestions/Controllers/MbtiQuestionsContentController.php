<?php

declare(strict_types=1);

namespace MbtiQuestions\Controllers;

// PHP
use InvalidArgumentException;

// App
use App\User;
use App\Question;
use App\Submission;
use App\Http\Controllers\Controller;

// Package
use MbtiQuestions\MbtiService;
use MbtiQuestions\SaveAnswersRequest;
use MbtiQuestions\Transformers\QuestionTransformer;

// Framework
use Illuminate\Http\JsonResponse;

// Aliases
use Log;
use View;

/**
 * This is our Page Controller that will be used for displaying and rendering
 * markdown pages by name.
 *
 * We extend from App controller because we want to inherit from the application
 * we are operating in.
 */
class MbtiQuestionsContentController extends Controller
{
    /**
     * Render and display page.
     *
     * @return JsonResponse
     */
    public function getQuestions(QuestionTransformer $transformer): JsonResponse
    {
        $questionsData  = Question::select('id', 'question')->get();
        $questionsItems = fractal($questionsData, $transformer)->toArray();

        return response()->json($questionsItems);
    }

    /**
     * Save answers submission and return relevant user and submission id.
     *
     * @param  SaveAnswersRequest $request
     * @param  MbtiService        $service
     *
     * @return JsonResponse
     */
    public function saveAnswers(SaveAnswersRequest $request, MbtiService $service): JsonResponse
    {
        // Gather input
        $answers = $request->input('answers');
        $email   = $request->input('email');

        // Save data and gather models
        $user       = $service->ensureUser($email);
        $submission = $service->saveSubmission($user, $answers);

        return response()->json([ 'user' => $user->id, 'submission' => $submission->id ]);
    }

    /**
     * Get the result for a single user submission.
     *
     * @param  User       $user
     * @param  Submission $submission
     *
     * @return JsonResponse
     */
    public function fetchResult(User $user, Submission $submission): JsonResponse
    {
        if ($user->id !== $submission->user_id) {
            throw new InvalidArgumentException('Submission and User do not match.');
        }

        return response()->json([ 'result' => $submission->result ]);
    }
}
