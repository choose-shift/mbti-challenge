<?php

declare(strict_types=1);

namespace MbtiQuestions;

// Vendor
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * PageMaker custom package service provider.
 */
class MbtiQuestionsServiceProvider extends ServiceProvider
{
    /**
     * Label our custom views namespace
     */
    public const CONFIG_NAMESPACE = 'mbti_questions';

    /**
     * The namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace for building
     * named routes.
     *
     * @var string
     */
    protected $namespace = 'MbtiQuestions\Controllers';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // If we dont call our parent our map method will not be called
        parent::boot();

        // Set Views path Hint for Custom Package - currently not being used
        // $this->loadViewsFrom(__DIR__ . '/Views', self::CONFIG_NAMESPACE);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::prefix('mbti')
             ->middleware('web')
             ->namespace($this->namespace)
             ->group(__DIR__ . '/routes.php');
    }
}
