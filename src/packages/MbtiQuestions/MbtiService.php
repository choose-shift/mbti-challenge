<?php

declare(strict_types=1);

namespace MbtiQuestions;

// PHP
use LogicException;

// App
use App\User;
use App\Answer;
use App\Question;
use App\Submission;

// Framework
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class MbtiService
{
    /**
     * MBTI Dimensions.
     *
     * We saving the labels here if we need it.
     *
     * @var array
     */
    public const DIMENSIONS = [
        'E' => 'Extraversion',
        'I' => 'Introversion',
        'S' => 'Sensing',
        'N' => 'Intuition',
        'T' => 'Thinking',
        'F' => 'Feeling',
        'J' => 'Judging',
        'P' => 'Perceiving',
    ];

    /**
     * List the dimension pairs / groups.
     *
     * @var  array
     */
    public const DIMENSION_PAIRS = [
        'EI',
        'SN',
        'TF',
        'JP',
    ];

    /**
     * Ensure a users exists else create new users and return the relevant user
     * model.

     * @param  string $email
     *
     * @return User
     */
    public function ensureUser(String $email): User
    {
        $user = User::where('email', $email)->first();

        if ($user) {
            return $user;
        }

        return User::create([ 'email' => $email ]);
    }

    /**
     * Save and assess submission answers.
     *
     * @param  User   $user
     * @param  array  $answers
     *
     * @return Submission
     */
    public function saveSubmission(User $user, array $answers): Submission
    {
        // Save submission
        $submission = $user->submissions()->create([
            // Result is pending on answers being saved and evaluated
            // @see assessSubmissionResult
            'result' => '',
        ]);

        // Build answers array
        $answers = collect($answers)
            ->map(function ($value, $questionId) use ($submission) {
                return [
                    'submission_id' => $submission->id,
                    'question_id'   => $questionId,
                    'value'         => $value,
                ];
            });

        // Bulk save answers
        $submission->answers()->createMany($answers->all());

        // Assess answers and save result
        $this->assessSubmissionResult($submission);

        return $submission;
    }

    /**
     * Assess results based on answers saved.
     *
     * @param  Submission $submission
     *
     * @return void
     */
    public function assessSubmissionResult(Submission $submission): void
    {
        $questions = Question::all()->keyBy('id');
        $resultMap = collect(array_keys(self::DIMENSIONS))
                     ->mapWithKeys(function ($value) {
                         return [ $value => 0 ];
                     });

        foreach ($submission->answers as $answer) {
            $question  = $questions->get($answer->question_id);
            $dimention = $this->calculateAnswerDimention($question, $answer);

            $count     = $resultMap->get($dimention);
            $resultMap = $resultMap->put($dimention, $count + $answer->value);
        }

        // Calculate final result string
        $result = $this->calculateFinalResult($resultMap);

        // Save result back to submission
        $submission->result = $result;
        $submission->save();
    }

    /**
     * Calculate a dimension for a single question.
     *
     * @param  Question $question
     * @param  Answer   $answer
     *
     * @return string
     */
    protected function calculateAnswerDimention(Question $question, Answer $answer): string
    {
        $dimension = $question->dimension;

        // Flip distention if direction is negative
        if ($question->direction == -1) {
            $dimension = strrev($dimension);
        }

        // answers 1-3 = first dimension
        if ($answer->value < 4) {
            return $dimension[0];
        }

        // answer 5-7 = second dimension
        if ($answer->value > 4) {
            return $dimension[1];
        }

        // Value is 4 / neutral = first dimension
        return $dimension[0];
    }

    /**
     * Based on the result map of questions, find the highest ranked dimension.
     *
     * @param  Collection $resultMap
     *
     * @return string
     */
    protected function calculateFinalResult(Collection $resultMap): string
    {
        $result = collect([]);

        foreach (self::DIMENSION_PAIRS as $pair) {
            // Get only the related dimensions based on Dimension pairs.
            $resultPair = $resultMap->filter(function ($value, $key) use ($pair) {
                return Str::contains($pair, $key);
            });

            // Validate we have all our data
            if ($resultPair->count() !== 2) {
                throw new LogicException(t('Expected 2 Result dimension pairs, received :count', [
                    ':count' => $resultPair->count(),
                ]));
            }

            // Keys
            $keyFirst  = $pair[0];
            $keySecond = $pair[1];

            // Values
            $valueFirst  = $resultPair->get($keyFirst);
            $valueSecond = $resultPair->get($keySecond);

            // If its a tie go with the first or if the first is bigger
            if ($valueFirst === $valueSecond || $valueFirst > $valueSecond) {
                $result = $result->push($keyFirst);
                continue;
            }

            // If we here the second dimension is bigger
            $result = $result->push($keySecond);
        }

        return $result->join('');
    }
}
