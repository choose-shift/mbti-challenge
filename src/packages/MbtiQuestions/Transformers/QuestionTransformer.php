<?php

namespace MbtiQuestions\Transformers;

// App
use App\Question;

// Vendor
use League\Fractal\TransformerAbstract;

/**
 * Api Model to JSON transformers for listing questions.
 */
class QuestionTransformer extends TransformerAbstract
{
    /**
     * Fractal Questions list request.
     *
     * @param  Question $item
     *
     * @return array
     */
    public function transform(Question $item): array
    {
        return [
            'id'       => $item->id,
            'question' => $item->question,
        ];
    }
}
