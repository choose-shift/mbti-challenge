<?php

/*
|--------------------------------------------------------------------------
| PageMaker Package Routes
|--------------------------------------------------------------------------
|
| Assumed: (see MbtiQuestionsServiceProvider.php)
|  - prefix     = mbti
|  - namespace  = MbtiQuestions\Controllers
|  - middleware = web
|
*/

// Fetch questions list
Route::get('questions', 'MbtiQuestionsContentController@getQuestions');

// Submit and save answers
Route::post('save', 'MbtiQuestionsContentController@saveAnswers');

// Get submission result
Route::get('{user}/result/{submission}', 'MbtiQuestionsContentController@fetchResult');
