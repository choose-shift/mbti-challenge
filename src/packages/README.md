# Domain Specific Custom Packages

The `/packages/` folder is used to implement a custom package system based on the [Gist Composer Sub Projects](https://gist.github.com/JFossey/4c1acc7512b0a585b42861e22a5bd58e)

Each subdirectory is a root-level namespace in the system, see entry in `composer.json` PSR-4 auto loading.

For these packages to be used in Laravel will require its own **Service Provider** to be loaded in `config\app.php`.
