<?php

declare(strict_types=1);

namespace PageMaker;

// PHP
use RuntimeException;

// Packages
use PageMaker\Contracts\MarkdownPageBuildingContract;

// Framework
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Config\Repository as ConfigContract;

// Vendor
use Parsedown;

/**
 * A page markdown rendering class that takes a page name and fetches and
 * renders the content markdown to html.
 */
class PageMaker implements MarkdownPageBuildingContract
{
    /**
     * The filesystem path from base path where the page content items are
     * stored.
     *
     * @var  string
     */
    protected $pagePath;

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The markdown parser instance.
     *
     * @var Parsedown
     */
    protected $parser;

    /**
     * Create a new markdown page maker instance with all dependencies injected
     * via the constructor.
     *
     * @param Filesystem     $files
     * @param Parsedown      $parser
     * @param ConfigContract $config
     *
     * @return void
     */
    public function __construct(Filesystem $files, Parsedown $parser, ConfigContract $config)
    {
        $this->files  = $files;
        $this->parser = $parser;

        // Set default path from config
        $this->setPagePath($config->get('page_maker.pages_path'));
    }

    /**
     * Change the path were pages are stored.
     *
     * @param  string $pagePath
     *
     * @return $this
     */
    public function setPagePath(string $pagePath): MarkdownPageBuildingContract
    {
        $this->pagePath = $pagePath;

        return $this;
    }

    /**
     * Get the currently set page path.
     *
     * @return string
     */
    public function getPagePath(): string
    {
        return $this->pagePath;
    }

    /**
     * Check if a page name exists.
     *
     * @param  string  $pageName
     *
     * @return boolean
     */
    public function hasPage(string $pageName): bool
    {
        $pageFile = $this->makePagePath($pageName);

        return $this->files->exists($pageFile);
    }

    /**
     * Get the contents of page using its page name.
     *
     * @param  string $pageName
     *
     * @return string
     */
    public function getPage(string $pageName): string
    {
        $pageFile = $this->makePagePath($pageName);

        return $this->files->get($pageFile);
    }

    /**
     * Render page markdown to html.
     *
     * @param  string $pageName
     *
     * @return string
     */
    public function render(string $pageName): string
    {
        if ($this->hasPage($pageName) === false) {
            throw new RuntimeException(t('Could not render page. Page `:name` not found.', [
                ':name' => $pageName,
            ]));
        }

        $markdown = $this->getPage($pageName);
        return $this->parser->text($markdown);
    }

    /**
     * Resolve the full absolute file system path to a given page name.
     *
     * @param  string $pageName
     *
     * @return string
     */
    protected function makePagePath(string $pageName): string
    {
        $pageFilename = $this->filename($pageName);

        return base_path($this->getPagePath()) . DIRECTORY_SEPARATOR . $pageFilename;
    }

    /**
     * Format a page name into the expected filename.
     *
     * In a URL we want to use kebab case EG: my-page for the name, but as a
     * file we want our view to be in snake case EG: my_page.md.
     *
     * @param  string $pageName
     *
     * @return string
     */
    protected function filename(string $pageName): string
    {
        // Transform: kebab => Camel => Snake
        return Str::snake(Str::camel($pageName)) . '.md';
    }
}
