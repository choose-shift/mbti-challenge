<?php

declare(strict_types=1);

namespace PageMaker\Controllers;

// PHP
use RuntimeException;

// Package
use PageMaker\Contracts\MarkdownPageBuildingContract;

// App
use App\Http\Controllers\Controller;

// Aliases
use Log;
use View;

/**
 * This is our Page Controller that will be used for displaying and rendering
 * markdown pages by name.
 *
 * We extend from App controller because we want to inherit from the application
 * we are operating in.
 */
class PageContentController extends Controller
{
    /**
     * Render and display page.
     *
     * @return string
     */
    public function view(MarkdownPageBuildingContract $pageMaker, $pageName): string
    {
        try {
            return $pageMaker->render($pageName);
        }
        // Only catch this specific exceptions, let all others fall through
        catch (RuntimeException $exception) {
            // Log original exception
            Log::error($exception);
            abort(404);
        }
    }
}
