<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Markdown Pages Location
     |--------------------------------------------------------------------------
     |
     | The path relative to app root where PageMaker markdown pages are kept.
     |
     */
    'pages_path' => 'resources/pages',

];
