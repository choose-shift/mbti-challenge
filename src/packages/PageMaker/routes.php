<?php

/*
|--------------------------------------------------------------------------
| PageMaker Package Routes
|--------------------------------------------------------------------------
|
| Assumed: (see PageMakerServiceProvider.php)
|  - prefix     = page
|  - namespace  = PageMaker\Controllers
|  - middleware = web
|
*/

// Render content pages
Route::get('{pageName}/view', 'PageContentController@view');
