<?php

declare(strict_types=1);

// App
namespace PageMaker\Contracts;

interface MarkdownPageBuildingContract
{
    /**
     * Change the path were pages are stored.
     *
     * @param  string $pagePath
     *
     * @return $this
     */
    public function setPagePath(string $pagePath): MarkdownPageBuildingContract;

    /**
     * Get the currently set page path.
     *
     * @return string
     */
    public function getPagePath(): string;

    /**
     * Check if a page name exists.
     *
     * @param  string  $pageName
     *
     * @return boolean
     */
    public function hasPage(string $pageName): bool;

    /**
     * Get the contents of page using its page name.
     *
     * @param  string $pageName
     *
     * @return string
     */
    public function getPage(string $pageName): string;

    /**
     * Render page markdown to html.
     *
     * @param  string $pageName
     *
     * @return string
     */
    public function render(string $pageName): string;
}
