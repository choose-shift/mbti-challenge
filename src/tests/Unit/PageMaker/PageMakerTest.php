<?php

declare(strict_types=1);

namespace Tests\Unit\PageMaker;

// PHP
use RuntimeException;

// Package
use PageMaker\PageMaker;
use PageMaker\Contracts\MarkdownPageBuildingContract;

// Tests
use Tests\TestCase;

/**
 * PHP Unit Tests for PageMaker service.
 */
class PageMakerTest extends TestCase
{
    /**
     * @var  string
     */
    public const TEST_PATH = 'tests/Unit/PageMaker/pages';

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPagePath(): void
    {
        $pageMaker = $this->getPageMakerInstance();

        $this->assertEquals($pageMaker->getPagePath(), self::TEST_PATH);
    }

    /**
     * Check different formats of page name using hasPage returns the expected results.
     *
     * @return void
     */
    public function testPageExists(): void
    {
        $pageMaker = $this->getPageMakerInstance();

        $this->assertTrue($pageMaker->hasPage('test-page'));
        $this->assertTrue($pageMaker->hasPage('test_page'));
        $this->assertTrue($pageMaker->hasPage('testPage'));

        $this->assertFalse($pageMaker->hasPage('test-page-missing'));
    }

    /**
     * Test that trying to render a page that does not exist throws an exception.
     *
     * @return void
     */
    public function testRenderFailure(): void
    {
        $this->expectException(RuntimeException::class);

        $pageMaker = $this->getPageMakerInstance();
        $pageMaker->render('test-page-missing');
    }

    /**
     * Test rendering a real page.
     *
     * @return void
     */
    public function testRender(): void
    {
        $expected  = '<h1>Test Page</h1>';
        $pageMaker = $this->getPageMakerInstance();
        $content   = $pageMaker->render('test-page');

        $this->assertEquals($content, $expected);
    }

    /**
     * Test our contract application service container binding.
     *
     * @return void
     */
    public function testContractBinding(): void
    {
        $instance = app(MarkdownPageBuildingContract::class);

        // We expect this to be a PageMaker instance but as this can be changed
        // we check for the contract
        $this->assertInstanceOf(MarkdownPageBuildingContract::class, $instance);
    }

    /**
     * Get a testable page maker instance.
     *
     * @return PageMaker
     */
    protected function getPageMakerInstance(): PageMaker
    {
        // Set our test pages path
        return app(PageMaker::class)->setPagePath(self::TEST_PATH);
    }
}
