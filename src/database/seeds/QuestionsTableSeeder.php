<?php

declare(strict_types=1);

// App
use App\Question;

// Framework
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $path = database_path('seeds/data/questions.csv');
        $data = collect_csv($path);

        $data->each(function ($row) {
            Question::create($row);
        });
    }
}
