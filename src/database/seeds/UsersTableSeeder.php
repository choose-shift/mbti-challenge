<?php

declare(strict_types=1);

// App
use App\User;

// Framework
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // We create a hard coded dummy example user
        User::create([
            'email' => 'example@chooseshift.com',
        ]);
    }
}
