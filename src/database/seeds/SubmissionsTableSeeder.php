<?php

declare(strict_types=1);

// App
use App\Answer;
use App\Submission;

// Framework
use Illuminate\Support\Arr;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class SubmissionsTableSeeder extends Seeder
{
    /**
     * Use default example user Id
     */
    public const USER_ID = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $path = database_path('seeds/data/test_cases.csv');
        $data = collect_csv($path, false)
                // Drop the headings row
                ->forget('0');

        // Drop the results row from data
        $results = collect($data->pop())
                   // Drop the headings row
                   ->forget('0');

        // Collate all the answers into submissions
        $submissions = $this->collateSubmissions($data);

        $submissions->each(function ($answers, $submissionIndex) use ($results) {
            // Create submission
            Submission::create([
                'user_id' => self::USER_ID,
                'result'  => $results->get($submissionIndex),
            ]);

            // Create related answers
            foreach ($answers as $answer) {
                Answer::create($answer);
            }
        });
    }

    /**
     * Raw test case data is columns per submission and rows for each question.
     *
     * We need to collate the data into answers grouped by submissions.
     *
     * @param  Collection $data
     *
     * @return Collection
     */
    protected function collateSubmissions(Collection $data): Collection
    {
        $submissions = collect([]);

        $data->each(function ($answers, $questionIndex) use (&$submissions) {
            // Drop headings
            Arr::forget($answers, '0');

            // Loop through row of answers
            foreach ($answers as $submissionIndex => $answer) {
                $submission = [
                    'submission_id' => $submissionIndex,
                    'question_id'   => $questionIndex,
                    'value'         => $answer,
                ];

                $submissions->push($submission);
            }
        });

        // Group all answers by submissions
        return $submissions->groupBy('submission_id');
    }
}
