#!/bin/sh

# Enter src
cd src

# Bower Install
bower install || exit ;

# Install node modules
yarn install || exit ;
npm run production

# Install PHP packages
composer install || exit ;

# Database
php artisan migrate --seed;

# Go back to project root
cd ../