#!/bin/sh

# ------------------------------------------------------------------------------
# Dev Helper - Run All Static Analysis and Test
# ------------------------------------------------------------------------------

## To make it more obvious to a dev seeing output that something important
## happed we will try and use ANSI escape codes

## Ref: https://misc.flogisoft.com/bash/tip_colors_and_formatting

## Reset        0;       Bold          1;
## Black        0;30m     Dark Gray     1;30m
## Red          0;31m     Light Red     1;31m
## Green        0;32m     Light Green   1;32m
## Brown/Orange 0;33m     Yellow        1;33m
## Blue         0;34m     Light Blue    1;34m
## Purple       0;35m     Light Purple  1;35m
## Cyan         0;36m     Light Cyan    1;36m
## Light Gray   0;37m     White         1;37m

RED='\033[0;31m';
GREEN='\033[0;32m';
BLUE='\033[0;34m';
NC='\033[0m'; # No Color

#-------------------------------------------------------------------------------

PREFIX="${GREEN}> \n> ${NC}";
SUFFIX="${GREEN}\n> \n\n${NC}";

#-------------------------------------------------------------------------------
# Handle working directory
#-------------------------------------------------------------------------------

# Dont lose the users current path location
USER_PWD=$(pwd);

# Change current directory to our script location
SCRIPT_PATH=$(dirname "$0")
cd "$SCRIPT_PATH" || exit ;

# Set our current location so we can use it
SCRIPT_PWD=$(pwd);

#-------------------------------------------------------------------------------
# PHP
#-------------------------------------------------------------------------------

# Run PHP syntax checking using linter
printf "${PREFIX}${BLUE}Running PHP Linter${NC} (Parallel Lint)${SUFFIX}" &&
src/vendor/bin/parallel-lint --exclude src/vendor --exclude src/node_modules --exclude src/storage --exclude src/libraries -j 10 ./src &&

# Run Coding Standards Enforcement
printf "${PREFIX}${BLUE}Running Easy Coding Standards${NC} (PHP Code Sniffer + PHP CS Fixer)${SUFFIX}" &&
src/vendor/bin/ecs check src/ &&

# Run PHPStan
printf "${PREFIX}${BLUE}Running PHPStan${NC} (Our PHP Policeman)${SUFFIX}" &&
src/vendor/bin/phpstan analyse &&

# Run PHP Unit
printf "${PREFIX}${BLUE}Running PHP Unit${NC} (Tests)${SUFFIX}" &&
src/vendor/bin/phpunit --configuration=src/phpunit.xml ;

#-------------------------------------------------------------------------------
# NODE
#-------------------------------------------------------------------------------

# Run Stylelint
printf "${PREFIX}${BLUE}Running Stylelint${NC} (CSS & Sass Linting)${SUFFIX}" &&
src/node_modules/stylelint/bin/stylelint.js --config-basedir=src "src/resources/**/*.scss" "src/resources/**/*.css" &&

# Run ESLint (from src/ - errors if run from project root?)
printf "${PREFIX}${BLUE}Running ESLint${NC} (JS Linting)${SUFFIX}" &&
cd src && node_modules/eslint/bin/eslint.js --config .eslintrc.yml resources/js/ && cd ../ &&

#-------------------------------------------------------------------------------

printf "${GREEN}All Good${NC}\n\n";

# Restore user location
cd "$USER_PWD";

#-------------------------------------------------------------------------------
